﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Aplicação_2_Sistemas_Distribuídos___REST.Models.Requests
{
    public class EventCancelBody
    {
        [Required(ErrorMessage = "Id field is required")]
        public int Id { get; set; }
    }
}
