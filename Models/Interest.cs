﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Aplicação_2_Sistemas_Distribuídos___REST.Models
{
    public class Interest
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Origin field is required")]
        public string Origin { get; set; }
        [Required(ErrorMessage = "Destination field is required")]
        public string Destination { get; set; }
        [Required(ErrorMessage = "Date field is required")]
        public string Date { get; set; }
        public string InterestedName { get; set; }
        [Required(ErrorMessage = "Offerer phone is required")]
        public string InterestedPhone { get; set; }
    }
}
