const driver_url = "https://localhost:5001/ride/"
const passenger_url = "https://localhost:5001/interest/"

// salva informações de login do passageiro
function setPassInfo() {
  const username = document.getElementById("pass_name").value
  const userphone = document.getElementById("pass_phone").value

  localStorage.setItem("Name", username)
  localStorage.setItem("Contact", username)

  window.location.href = "./passenger/passenger.html"
}

// salva informações de login do motorista
function setDriverInfo() {
  const username = document.getElementById("driver_name").value
  const userphone = document.getElementById("driver_phone").value

  localStorage.setItem("Name", username)
  localStorage.setItem("Contact", username)

  window.location.href = "./driver/driver.html"
}

// atualiza nome do passageiro nas próximas telas
function updatePassengerPage() {
  const username = localStorage.getItem("Name")
  document.getElementById("id_passenger").innerHTML = "Welcome, " + username
}

// atualiza nome do motorista nas próximas telas
function updateDriverPage() {
  const username = localStorage.getItem("Name")
  document.getElementById("id_driver").innerHTML = "Welcome, " + username
}

// envia as informações do interesse para o servidor
async function postInterest() {
  const p_name = localStorage.getItem("Name")
  const p_phone = localStorage.getItem("Contact")

  const p_origin = document.getElementById("p_origin").value
  console.log(p_origin)
  const p_destiny = document.getElementById("p_destiny").value
  const p_date = document.getElementById("p_date").value
  // const p_seats = document.getElementById("pass-seats")

  var payload = {
    "Origin": p_origin,
    "Destination": p_destiny,
    "Date": p_date,
    "InterestedName": p_name,
    "InterestedPhone": p_phone
  }
  console.log(payload)

  const response = await fetch(passenger_url + "register", {
    method: "POST",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  })

  const data = await response.json();
  console.log(data)
  var interest = {
    "Id": data,
    "Origin": p_origin,
    "Destination": p_destiny,
    "Date": p_date,
  }

  var ride = getThisRide(interest.Origin, interest.Destination, interest.Date)

  localStorage.setItem("interest", JSON.stringify(interest))
  localStorage.setItem("ride", JSON.stringify(ride))
  //console.log("len " + interests.length)

  var eventRide = new EventSource("https://localhost:5001/ride/subscribe"
    + "?origin=" + p_origin + "&destination="
    + p_destiny + "&date=" + p_date)
  eventRide.onmessage = function (message) {
    {
      if (window.confirm(message.data) === true && message.data.length > 25) {
        eventRide.close()
      }
    }
  }

}

// mostra corrida na tela
function showRide(data) {
  var mainContainer = document.getElementById("rides");
  console.log("data len: ", data.length)
  if (data.length > 0) {
    for (var i = 0; i < data.length; i++) {
      var div = document.createElement("div");
      div.innerHTML = "Name: " + data[i].offererName + ";<br>Contact: " + data[i].offererPhone
        + ";<br>Origin: " + data[i].origin + ";<br>Destinarion: " + data[i].destination + ";<br>Date: " + data[i].date
      mainContainer.appendChild(div);
    }
  }
  else {
    var div = document.createElement("div");
    div.innerHTML = "No interest found"
    mainContainer.appendChild(div);
  }
}

// solicita as informações das corridas para o servidor
async function getRide() {
  var interest = JSON.parse(localStorage.getItem("interest"))

  console.log("interest: ", interest)

  var payload = {
    "Origin": interest.Origin,
    "Destination": interest["Destination"],
    "Date": interest["Date"]
  }

  const response = await fetch(driver_url + "find", {
    method: "POST",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  });
  const data = await response.json();
  console.log(data)
  showRide(data);
}

// solicita as informações da corrida atual para o servidor
async function getThisRide(origin, destiny, date) {
  var payload = {
    "Origin": origin,
    "Destination": destiny,
    "Date": date
  }
  console.log(payload)

  const response = await fetch(driver_url + "find", {
    method: "POST",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  });
  const data = await response.json();
  console.log("data get this: ", data)
  showRide(data);
}

// deleta um interesse no servidor
async function deleteInterest() {
  let in_id = window.prompt("Which ride?")
  console.log(in_id)

  var payload = {
    "id": parseInt(in_id, 10)
  }

  const response = await fetch(passenger_url + "cancel", {
    method: "DELETE",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  })
  const data = await response
  console.log(data)
}

// envia as informações da corrida para o servidor
async function postRide() {
  const d_name = localStorage.getItem("Name")
  const d_phone = localStorage.getItem("Contact")

  const d_origin = document.getElementById("d_origin").value
  const d_destiny = document.getElementById("d_destiny").value
  const d_date = document.getElementById("d_date").value
  const d_seats = document.getElementById("d_seats").value

  var payload = {
    "Origin": d_origin,
    "Destination": d_destiny,
    "Date": d_date,
    "Passengers": parseInt(d_seats, 10),
    "OffererName": d_name,
    "OffererPhone": d_phone
  }
  console.log(payload)

  const response = await fetch(driver_url + "register", {
    method: "POST",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  })

  const data = await response.json();
  console.log(data)

  var ride = {
    "Id": data,
    "Origin": d_origin,
    "Destination": d_destiny,
    "Date": d_date,
  }

  var interest = getThisInterest(ride.Origin, ride.Destination, ride.Date)

  localStorage.setItem("interest", JSON.stringify(interest))
  localStorage.setItem("ride", JSON.stringify(ride))

  var eventInterest = new EventSource("https://localhost:5001/interest/subscribe" + "?origin=" + d_origin + "&destination=" + d_destiny + "&date=" + d_date)
  eventInterest.onmessage = function (message) {
    {
      if (window.confirm(message.data) === true && message.data.length > 25) {
        eventInterest.close()
      }
    }
  }
}

// mostra os interessados em carona na tela
function showInterest(data) {
  var mainContainer = document.getElementById("interests");
  console.log("data len: ", data.length)
  if (data.length > 0) {
    for (var i = 0; i < data.length; i++) {
      var div = document.createElement("div");
      div.innerHTML = "Name: " + data[i].interestedName + ";<br>Contact: " + data[i].interestedPhone
        + ";<br>Origin: " + data[i].origin + ";<br>Destinarion: " + data[i].destination + ";<br>Date: " + data[i].date
      mainContainer.appendChild(div);
    }
  }
  else {
    var div = document.createElement("div");
    div.innerHTML = "No interest found"
    mainContainer.appendChild(div);
  }
}

// solicita as informações dos interesses para o servidor
async function getInterest() {
  var interest = JSON.parse(localStorage.getItem("ride"))

  console.log(interest)
  console.log(interest.Origin)
  console.log(interest["Destination"])

  var payload = {
    "Origin": interest.Origin,
    "Destination": interest["Destination"],
    "Date": interest["Date"]
  }

  const response = await fetch(passenger_url + "find", {
    method: "POST",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  });
  const data = await response.json();
  console.log(data)

  // manda pra próxima tela tudo o que achar
  showInterest(data)
}

// solicita as informações do interesse atual para o servidor
async function getThisInterest(origin, destiny, date) {
  var payload = {
    "Origin": origin,
    "Destination": destiny,
    "Date": date
  }
  console.log(payload)

  const response = await fetch(passenger_url + "find", {
    method: "POST",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  });
  const data = await response.json();
  console.log(data)
  showInterest(data)
}

// deleta uma corrida do servidor
async function deleteRide() {
  let in_id = window.prompt("Which ride?")
  console.log(in_id)

  var payload = {
    "Id": parseInt(in_id, 10)
  }

  const response = await fetch(driver_url + "/cancel", {
    method: "DELETE",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  })
  const data = await response.json()
  console.log(data)
}





