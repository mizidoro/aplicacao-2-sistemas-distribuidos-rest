﻿using Aplicação_2_Sistemas_Distribuídos___REST.Data;
using Aplicação_2_Sistemas_Distribuídos___REST.Models;
using Aplicação_2_Sistemas_Distribuídos___REST.Models.Requests;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicação_2_Sistemas_Distribuídos___REST.Controllers
{
    [EnableCors]
    [ApiController]
    [Route("ride")]
    public class RideController : ControllerBase
    {
        //This method exposes a way to clients find rides that are already registered on the system
        [HttpPost]
        [Route("find")]
        public async Task<ActionResult<List<Ride>>> FindRide([FromServices] DataContext context, [FromBody] EventBody rideInfo)
        {
            return context.Rides.Where(r => r.Origin.Equals(rideInfo.Origin) && r.Destination.Equals(rideInfo.Destination) && r.Date.Equals(rideInfo.Date)).ToList();
        }

        //This method is responsible to register clients ride offers to the database
        [HttpPost]
        [Route("register")]
        public async Task<ActionResult<int>> RegisterRide([FromServices] DataContext context, [FromBody] Ride ride)
        {
            if (ModelState.IsValid)
            {
                context.Rides.Add(ride);
                await context.SaveChangesAsync();
                return ride.Id;
            }
            else
            {
                //it return a bad request if the model received is different that is expected
                return BadRequest(ModelState);
            }
        }

        //With this method users can cancel their ride offers
        [HttpDelete]
        [Route("cancel")]
        public async Task<ActionResult> CancelRide([FromServices] DataContext context, [FromBody] EventCancelBody evt)
        {
            var ride = context.Rides.FirstOrDefault(r => r.Id.Equals(evt.Id));
            if (ride != null)
            {
                context.Rides.Remove(ride);
                await context.SaveChangesAsync();
                //it returns NoContent as no further actions need to be done
                return NoContent();
            }
            return Content("No ride with the specified Id was found");
        }

        //Subscribe clients to get notified when a new ride is registered
        [HttpGet]
        [Route("subscribe")]
        public async Task<ActionResult> SubscribeForRideNotifications([FromServices] DataContext context, [FromQuery] string origin, [FromQuery] string destination, [FromQuery] string date)
        {
            Response.Headers.Add("Content-Type", "text/event-stream");

            Ride rideFound = null;

            while (rideFound == null)
            {
                rideFound = context.Rides.FirstOrDefault(i => i.Origin.Equals(origin) && i.Destination.Equals(destination) && i.Date.Equals(date));

                if (rideFound != null)
                {
                    string message = "data: " + "Ride found!\n" + 
                                     "Origin: " + rideFound.Origin + "\n" +
                                     "Destination: " + rideFound.Destination + "\n" +
                                     "Date: " + rideFound.Date + "\n" +
                                     "Name: " + rideFound.OffererName + "\n" +
                                     "Phone: " + rideFound.OffererPhone + "\n\n";
                    byte[] messageBytes = ASCIIEncoding.ASCII.GetBytes(message);
                    await Response.BodyWriter.WriteAsync(messageBytes);
                    await Response.BodyWriter.FlushAsync();
                }

                await Task.Delay(TimeSpan.FromSeconds(1));
            }

            return Ok();
        }
    }
}
