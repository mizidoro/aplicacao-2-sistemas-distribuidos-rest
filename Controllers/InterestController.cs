﻿using Aplicação_2_Sistemas_Distribuídos___REST.Data;
using Aplicação_2_Sistemas_Distribuídos___REST.Models;
using Aplicação_2_Sistemas_Distribuídos___REST.Models.Requests;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicação_2_Sistemas_Distribuídos___REST.Controllers
{
    [EnableCors]
    [ApiController]
    [Route("interest")]
    public class InterestController : ControllerBase
    {
        //This method exposes a way for clients to find people interested in some ride
        [HttpPost]
        [Route("find")]
        public async Task<ActionResult<List<Interest>>> FindInterest([FromServices] DataContext context, [FromBody] EventBody interestInfo)
        {
            return context.Interests.Where(r => r.Origin.Equals(interestInfo.Origin) && r.Destination.Equals(interestInfo.Destination) && r.Date.Equals(interestInfo.Date)).ToList();
        }

        //This method is responsible for register a ride interest in the database
        [HttpPost]
        [Route("register")]
        public async Task<ActionResult<int>> RegisterInterest([FromServices] DataContext context, [FromBody] Interest interest)
        {
            if (ModelState.IsValid)
            {
                context.Interests.Add(interest);
                await context.SaveChangesAsync();
                return interest.Id;
            }
            else
            {
                //It returns a bad request if the received body is different from what it is expected
                return BadRequest(ModelState);
            }
        }

        //This method allows clients to cancel their ride interests using the interest id as a parameter given that the id is unique
        [HttpDelete]
        [Route("cancel")]
        public async Task<ActionResult> CancelInterest([FromServices] DataContext context, [FromBody] EventCancelBody evt)
        {
            var interest = context.Interests.FirstOrDefault(i => i.Id.Equals(evt.Id));
            if (interest != null)
            {
                context.Interests.Remove(interest);
                await context.SaveChangesAsync();
                //it returns NoContent as no further actions need to be done
                return NoContent();
            }
            return Content("No interest with the specified Id was found");
        }

        //Subscribe clients to get notified when a new interest is registered
        [HttpGet]
        [Route("subscribe")]
        public async Task<ActionResult> SubscribeForInterestNotifications([FromServices] DataContext context, [FromQuery] string origin, [FromQuery] string destination, [FromQuery] string date)
        {
            Response.Headers.Add("Content-Type", "text/event-stream");

            Interest interestFound = null;

            while (interestFound == null)
            {
                interestFound = context.Interests.FirstOrDefault(i => i.Origin.Equals(origin) && i.Destination.Equals(destination) && i.Date.Equals(date));

                if (interestFound != null)
                {
                    string message = "data: " + "Interest found!\n" +
                                     "Origin: " + interestFound.Origin + "\n" +
                                     "Destination: " + interestFound.Destination + "\n" +
                                     "Date: " + interestFound.Date + "\n" +
                                     "Name: " + interestFound.InterestedName + "\n" +
                                     "Phone: " + interestFound.InterestedPhone + "\n\n";
                    byte[] messageBytes = ASCIIEncoding.ASCII.GetBytes(message);
                    await Response.BodyWriter.WriteAsync(messageBytes);
                    await Response.BodyWriter.FlushAsync();
                }

                await Task.Delay(TimeSpan.FromSeconds(1));
            }

            return Ok();
        }
        public class Teste
        {
            [FromHeader]
            public string teste { get; set; }
        }
    }
}
