﻿using Aplicação_2_Sistemas_Distribuídos___REST.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aplicação_2_Sistemas_Distribuídos___REST.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<Ride> Rides { get; set; }
        public DbSet<Interest> Interests { get; set; }
    }
}
